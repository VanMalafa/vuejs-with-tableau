var viz;

window.onload = function() {
  var vizDiv = document.getElementById("myViz");

  var vizURL =
    "https://public.tableau.com/views/Jugadoresdefutbol/Dashboard1?:embed=y&:toolbar=no&:embed_code_version=3&:loadOrderID=0&:display_count=yes";
  var options = {
    onFirstInteractive: function() {
      console.log("Run this code when the viz has finished loading.");
    }
  };
  viz = new tableau.Viz(vizDiv, vizURL, options);
};

// Content below is to resolve warning for missing 'tableau' object
var tableau;
